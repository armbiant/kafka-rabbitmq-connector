package io.gitlab.themicroservicedungeon;

import io.smallrye.mutiny.Multi;
import io.smallrye.reactive.messaging.kafka.api.IncomingKafkaRecordMetadata;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata;
import io.vertx.core.json.JsonObject;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.Headers;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.eclipse.microprofile.reactive.messaging.*;

import javax.enterprise.context.ApplicationScoped;
import org.jboss.logging.Logger;

@ApplicationScoped
public class KafkaRabbitMQConnectorApplication {
    private final String PLAYER_ID_HEADER_KEY = "playerId";

    // We could do that to avoid minor mistakes in the services. At the moment it is not necessary.
    // private final String[] PLAYER_ID_HEADER_KEYS = { "playerId", "player_id", "player-id", "playerID", "player" };
    private final String[] PLAYER_ID_HEADER_KEYS = { PLAYER_ID_HEADER_KEY };
    private static final Logger LOG = Logger.getLogger(KafkaRabbitMQConnectorApplication.class);

    @Incoming("inc-game-messages")
    @Outgoing("out-game-messages")
    public Multi<Message<String>> bridge(Message<String> message) {
        var recordMetadata = message.getMetadata(IncomingKafkaRecordMetadata.class).orElseThrow();
        var topic = recordMetadata.getTopic();

        if(LOG.isDebugEnabled()) {
            LOG.debug("Received Kafka message in Topic: " + topic);
        }

        var playerHeaders = extractPlayerHeader(recordMetadata.getHeaders());
        if(LOG.isDebugEnabled()) {
            var stringRep = playerHeaders.stream()
                    .map(header -> "Key: " + header.key() + " / Value: " + new String(header.value()))
                    .toList();
            if(stringRep.size() > 0) {
                LOG.debug("The message contains the following player headers: " + String.join(",",
                    stringRep));
            } else {
                LOG.debug("The message did not contain player headers");
            }
        }

        var routingKeys = determineRoutingKeys(playerHeaders);
        if(LOG.isDebugEnabled()) {
            LOG.debug("The message will be forwarded with the following routing keys: " + String.join(",", routingKeys));
        }
        var messages = new ArrayList<Message<String>>();

        for(var routingKey : routingKeys) {
            var headers = headersToMap(recordMetadata.getHeaders());
            // We remove every player header, as we do not want to forward them to the player. That
            // would make other player IDs public at this point.
            headers = removePlayerHeaders(headers);

            // We add the routing key (playerId) as a header
            headers.put(PLAYER_ID_HEADER_KEY, routingKey.getBytes());

            // The error topic is a special case where we wan't to update the common type, to give players the opportunity 
            // for filtering out all errors and as we tend to share the same error type and distinguish by using error codes
            // this should not be a problem.
            if(topic.equals("error")) {
                headers.put("type", "error".getBytes());
            }

            // Let's just put the original kafka topic in the headers aswell. We might need it
            headers.put("kafka-topic", topic.getBytes());

            // Instead of casting Map<String, byte[]> to Map<String, Object> we just create a new map.
            // This is not the most efficient way, but it might be the easiest to understand and most
            // readable. The JIT compiler should be able to optimize this.
            Map<String, Object> rabbitMQHeaders = new HashMap<>(headers);
            final OutgoingRabbitMQMetadata metadata = new OutgoingRabbitMQMetadata.Builder()
                .withHeaders(rabbitMQHeaders)
                .withRoutingKey(routingKey)
                .withTimestamp(ZonedDateTime.now())
                .withContentType("application/json")
                .build();

            // Note: We're not parsing json here
            messages.add(message
                .withMetadata(Metadata.of(metadata))
                .withPayload(message.getPayload()));
        }

        // TODO: We should do it in more reactive way
        return Multi.createFrom().iterable(messages);
    }

    private Map<String, byte[]> headersToMap(Headers headers) {
        Map<String, byte[]> map = new HashMap<>();
        for (Header header : headers) {
            map.put(header.key(), header.value());
        }
        return map;
    }

    private Map<String, byte[]> removePlayerHeaders(Map<String, byte[]> headers) {
        Map<String, byte[]> processed = new HashMap<>(headers);
        for (String s : PLAYER_ID_HEADER_KEYS) {
            processed.remove(s);
        }
        return processed;
    }

    private List<Header> extractPlayerHeader(Headers headers) {
        return StreamSupport.stream(headers.spliterator(), false)
            .filter(header -> Stream.of(PLAYER_ID_HEADER_KEYS).anyMatch(key -> key.equals(header.key())))
            .toList();
    }

    private List<String> determineRoutingKeys(List<Header> headers) {
        if (headers.isEmpty()) {
            return List.of("public");
        }
        return headers.stream()
            .map(header -> new String(header.value()))
            .toList();
    }
}
