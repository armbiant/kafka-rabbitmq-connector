package io.gitlab.themicroservicedungeon;

import static org.junit.jupiter.api.Assertions.*;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.kafka.InjectKafkaCompanion;
import io.smallrye.reactive.messaging.kafka.companion.KafkaCompanion;
import org.junit.jupiter.api.Test;

@QuarkusTest
class KafkaRabbitMQConnectorApplicationTest {

  @InjectKafkaCompanion
  KafkaCompanion companion;

  @Test
  void shouldCopyHeaders() {
    // I have no clue how to test this properly
  }

  @Test
  void shouldUsePlayerIdAsBindingKey() {
    // I have no clue how to test this properly

  }

  @Test
  void shouldFallbackToPublicAsBindingKey() {
    // I have no clue how to test this properly

  }
}
